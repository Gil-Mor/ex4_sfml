#pragma once
#include "Button.h"
#include "Tile.h"

class DrawingButton : public Button
{
public:

    DrawingButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        Tile* type,
        LevelEditor& editor);

    DrawingButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        const sf::Color& color,
        Tile* type,
        LevelEditor& editor);

    ~DrawingButton();

    void action();

private:

    Tile* _type;

};

