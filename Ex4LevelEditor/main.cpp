/* main of SaveTheKing LevelEditor. */
#include "LevelEditor.h"

int main()
{
    // request size inside level editor cotr because anyway has to also
    // display request message in SFML window...
    LevelEditor editor;
    if (editor.getSuccessful())
        editor.run();

    return 0;
}