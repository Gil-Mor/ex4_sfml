#pragma once

/* Controller For the level editor. constructs drawBoard and menu
and utilities used by them.
*/
#include <SFML\Graphics.hpp>
#include "Menu.h"
#include "DrawBoard.h"
#include "Shape.h"
#include <string>

using std::string;

class LevelEditor
{
public:

    LevelEditor();

    ~LevelEditor();

    /* main loop handles input events from he user.*/
    void run();

    /* calls board and menu's to draw them selfes*/
    void draw();

    /* Drawing Buttons place the type of the tile they're holding
    "under" the mouse cursor. this is whats drawn on the board.*/
    void placeUnderCursor(Tile* tile);

    /* Get the Tile that's currently unter the cursor.*/
    Tile* getUnderCorsur() const;

    /* return true to main if all initializations were successful.*/
    bool getSuccessful() const;

    /* different modes according to clicked buttons..*/
    void setChoiceMode();
    void setDeletionMode();
    void setDrawingMode();

    /* delete button activates deletion of previousley chosen objects*/
    void activateDeletionClick();

    /* Save the board to a file. */
    void saveToFile();

    /* initialize a new board. asks the user for new size.*/
    void newBoard();

    /* changes the color of the next walls that will
    be drawn.
    */
    void changeWallColor();

    /* get the currnet wall color.
    wall color button uses this to change it's own color.*/
    sf::Color getWallColor() const;

private:

    /* holds Tiles that can be drawn to the board.
    * the buttons can set their proporties and 
    * tell this LevelEditor what to draw next..
    */
    vector<Tile*> _gameObjects;
    
    // ------------------ RUN -------------------------

    // pointer to a Tile type which is currently "under the cursor"
    // meaning the user chose it from the menu.
    Tile* _underCursorTile;

    /* calls menu's and board's handle function.*/
    void handleMouseMovment(const sf::Vector2i& mouse);
    void handleMouseClick(const sf::Vector2i& mouse);
    //--------------------run--------------------------


    //============= INITIALIZATION =============================

    /* initialize everything except loading textures.*/
    void initAll();

    /* load textures from files.*/
    bool loadTextures();

    /* Initalize game objects samples.
    menu and board use these samples for information about the objects.
    it's like packages of information.
    instead of having different parts like types enum
    and textures vectr etc...*/
    void initGameObjects();


    /* laod menu textures and boards textures seperatley.*/
    bool loadMenuTextures();
    bool loadBoardTextures();

    // indicates if the initialization was succesful
    // so that main will know wheter to run the editor
    // after trying to initialize it.
    bool _successful;

    /* setsthe variable telling if initialization was succesful.*/
    void setSuccessful(bool);


    // --------------- MENU STUFF ----------------------

    /* Menu object.*/
    Menu _menu;
    bool initMenu();

    // saving textures pointers here for easier deallocation.
    vector<const sf::Texture*> _menuTextures;
    // -------------------menu-----------------------



    //--------------- DRAWBORD STUFF ------------------

    /* board object.
    theboard the user draws on.*/
    DrawBoard _board;
    
    /* differnet modes according to pressed button.*/
   enum Modes_E
    {
         DRAWING_M, 
         CHOICE_M, 
         DELETE_M
    };
   Modes_E _mode;

   /*initialize the modes.*/
    void initModes();

    /*initialize the draw board.*/
    bool initDrawBoard();

    /* get the level size from the user.*/
    sf::Vector2u getLevelSize() const;

    // saving textures pointers here for easier deallocation.
    vector<const sf::Texture*> _boardTextures;

    /* string of the object names that's used also to write to the file.*/
    vector<string> _objectsNames;
    void removePostfixFromNmaes();
    bool restartEditor();
    //------------------drawboard---------------------



    //--------------- MENU / BOARD -------------------

    /* load textures names from a file into a vector. */
    bool loadTexturesNames(vector<string>& v, const string& file) const;

    /* put textures in a vector. the vector is used to initialize objects.*/
    bool LevelEditor::texturesToVector(const vector<string>& names,
        vector<const sf::Texture*>& textures);


    // --- WALL COLORS ---

    // struct that manages wall colors..
    // problem is that both menu and board use
    // the same wall colors and they need to be
    // syncronized.
    struct WallColors
    {
        enum WallColors_E
        {
            GRAY_C = 0,
            MAROON_C,
            BLUE_C,
            NUM_OF_COLORS_C
        };

        unsigned int index;

        vector<sf::Color> array;

    } _wallColors;

    void initWallColors();
    //----------------menu / board--------------------
    

    //----------------- WINDOW STUFF -----------------
    sf::RenderWindow _window;
    void initWindow();
    // save window size in vector2u to worc with 2u's Vectors
    sf::Vector2u sizeU;

    // save window size in vector2f to worc with 2f's Vectors
    sf::Vector2f sizeF;
    void initSizeAuxis();

    // displays a message in the sfml window
    void displayMessage(const string& message);
    //---------------------window----------------------

    //==============initialization===============================

};

