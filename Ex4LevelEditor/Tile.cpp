#include "Tile.h"

Tile::Tile()
    : Shape()
{
}


Tile::Tile(const sf::Texture* tex,
    TileType::Tiles_E type,
    unsigned int row,
    unsigned int col)
    : _type(type), _trueTexture(tex), _trueColor(TEXTURED_COLOR),
    _boardPos({ row, col }),
    _chosen(false), _occupied(false),
    Shape({ 0, 0 }, { 0, 0 }, tex)
{
}

Tile::Tile(const sf::Texture* tex,
    const sf::Color& color,
    TileType::Tiles_E type,
    unsigned int row,
    unsigned int col)
    : _type(type), _trueTexture(tex), _trueColor(color),
    _boardPos({row, col}),
    _chosen(false), _occupied(false),
    Shape({ 0, 0 }, { 0, 0 }, tex, color)
{
}


Tile::Tile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Color& color,
    TileType::Tiles_E type,
    unsigned int row,
    unsigned int col)
    : _type(type), _trueTexture(nullptr), _trueColor(color),
    _boardPos({row, col}),
     _chosen(false), _occupied(false),
    Shape(size, position, nullptr, color)
{
}

Tile::Tile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* tex,
    TileType::Tiles_E type,
    unsigned int row,
    unsigned int col)
    : _type(type), _trueTexture(tex), _trueColor(TEXTURED_COLOR),
    _boardPos({ row, col }),
   _chosen(false), _occupied(false),
    Shape(size, position, tex)
{
}

Tile::Tile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* tex,
    const sf::Color& color,
    TileType::Tiles_E type,
    unsigned int row,
    unsigned int col)
    : _type(type), _trueTexture(tex), _trueColor(color),
    _boardPos({ row, col }),
    _chosen(false), _occupied(false),
    Shape(size, position, tex, color)
{
}

void Tile::restore()
{
    setTexture(_trueTexture);
    setColor(_trueColor);
}

void Tile::setTrueTexture(const sf::Texture* tex)
{
    _trueTexture = tex;
}

void Tile::setTrueColor(const sf::Color color)
{
    _trueColor = color;
}

void Tile::setRow(unsigned int row)
{
    _boardPos.row = row;
}
void Tile::setCol(unsigned int col)
{
    _boardPos.col = col;
}

unsigned int Tile::getRow() const
{
    return _boardPos.row;
}

unsigned int Tile::getCol() const
{
    return _boardPos.col;
}

bool Tile::transformable() const
{
    return false;
}

unsigned int Tile::getLength() const
{
    return 1;
}

void Tile::setOccupied(bool o)
{
    _occupied = o;
}

bool Tile::getOccupied() const
{
    return _occupied;
}

void Tile::setChosen(bool c)
{
    _chosen = c;
}

bool Tile::getChosen() const 
{
    return _chosen;
}

void Tile::setType(TileType::Tiles_E type)
{
    _type = type;
}

TileType::Tiles_E Tile::getType() const
{
    return _type;
}


Tile::~Tile()
{
}

