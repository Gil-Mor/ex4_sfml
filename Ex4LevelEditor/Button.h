#pragma once
#include "Shape.h"
#include "Tile.h"
#include <string>
#include <iostream>

using std::cout;

class LevelEditor;

class Button : public Shape
{
public:

    Button(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
         LevelEditor& editor);

    Button(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        const sf::Color& color,
         LevelEditor& editor);

    virtual ~Button();

    /* set pressed state. */
    void setPressed(bool p);
    /* get pressed state. */
    bool getPressed() const;

    /* Pure virtual function overridden in all buttons.*/
    virtual void action()=0;

protected:

    /* Every button has a reference to LevelEditor.
    This is how the different buttons execute commends.*/
     LevelEditor& _editor;

     /* true if the button is pressed by the mouse. */
     bool _pressed;

    
};

