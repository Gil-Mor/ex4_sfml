#include "DeleteButton.h"
#include "LevelEditor.h"

DeleteButton::DeleteButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

DeleteButton::~DeleteButton()
{
}

void DeleteButton::action()
{
    _editor.setDeletionMode();
    
    _editor.activateDeletionClick();

}
