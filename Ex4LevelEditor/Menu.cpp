#include "Menu.h"
#include <iostream>
#include"ChoiceButton.h"
#include "DeleteButton.h"


#include "NewButton.h"

#include "RotateButton.h"
#include "SaveButton.h"
#include "ShrinkButton.h"
#include "StretchButton.h"

#include "WallColorButton.h"

#include "DrawingButton.h"

/*
#include "TeleportButton.h"
#include "WallButton.h"
#include "FireButton.h"
#include "WarriorButton.h"
#include "GateButton.h"
#include "KeyButton.h"
#include "KingButton.h"
#include "MageButton.h"
#include "ThiefButton.h"
#include "ThroneButton.h"
#include "OrcButton.h"
*/

#include "LevelEditor.h"

using std::cout;
using std::cerr;
using std::endl;


const float BUTTON_SIZE = 60;
const float BUTTON_BG_SIZE = 80;

const sf::Color BUTTON_FOCUS_COLOR = { 160, 32, 240, 250 };
const sf::Color BUTTON_NO_FOCUS_COLOR = sf::Color::White;

const sf::Color BUTTON_PRESSED_FOCUS_COLOR = { 200, 32, 180, 250 };

const sf::Color BUTTON_BG_NO_FOCUS_COLOR = { 255, 100, 255, 255 };
const sf::Color BUTTON_BG_FOCUS_COLOR = { 50, 232, 240, 250 };

const int NUM_OF_BUTTONS_IN_ROW = 3;



const string MENU_BG_FILE = "../SourceTextures/menu_background.png";
const string BUTTONS_BG_FILE = "../SourceTextures/buttons_background.png";

Menu::Menu(const sf::Vector2f& size,
    const sf::Vector2f& position,
    LevelEditor& editor)
    : _editor(editor),
    Shape(size, position, nullptr),
    _buttonSize(BUTTON_SIZE, BUTTON_SIZE)
{

    _buttons.resize(NUM_OF_BUTTONS_E, nullptr);
    setBackGround();
    setButtonsBackground();
}

void Menu::setGameObjects(const vector<Tile*>& gameObjects)
{
    for (unsigned int i = 0; i < gameObjects.size(); ++i)
    {
        _gameObjects.push_back(gameObjects[i]);
    }
}

Menu::~Menu()
{
    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        delete _buttons[i];
        _buttons[i] = nullptr;
    }

}

void Menu::handleMouseMovment(const sf::Vector2i& mousePosition)
{
    cleanMouse();

    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        _buttons[i]->setInFocus(false);
        if (_buttons[i]->mouseHover(mousePosition))
        {
            _buttons[i]->setInFocus(true);
        }
    }
}

void Menu::handleMouseClick(const sf::Vector2i& mousePosition,
    Tile* underCursor)
{
    cleanMouse();

    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        if (_buttons[i]->mouseHover(mousePosition))
        {
            _buttons[i]->setPressed(true);
            _buttons[i]->action();
        }
    }

}


void Menu::setButtons(const vector<const sf::Texture*>& textures)
{
    sf::Vector2f gap(22, 25);
    sf::Vector2f pos = getPosition() + gap;


    unsigned int alreadyInRow = 0;


    _buttons[Buttons_E::NEW_BUTTON_E] =
        (new NewButton(_buttonSize, pos, 
        textures[ButtonsTextures_E::NEW_BUTTON_TEX_E], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::SAVE_BUTTON_E] =
        (new SaveButton(_buttonSize, pos, 
        textures[ButtonsTextures_E::SAVE_BUTTON_TEX_E], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::CHOICE_BUTTON_E] =
        (new ChoiceButton(_buttonSize, pos, 
        textures[ButtonsTextures_E::CHOICE_BUTTON_TEX_E], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::DELETE_OBJECT_BUTTON_E] =
        (new DeleteButton(_buttonSize, pos,
        textures[ButtonsTextures_E::DELETE_OBJECT_BUTTON_TEX_E], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::ROTATE_BUTTON_E] =
        (new RotateButton(_buttonSize, pos, 
        textures[ButtonsTextures_E::ROTATE_BUTTON_TEX_E], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::SHRINK_OBJECT_BUTTON_E] =
        (new ShrinkButton(_buttonSize, pos, 
        textures[ButtonsTextures_E::SHRINK_OBJECT_BUTTON_TEX_E], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::STRETCH_OBJECT_BUTTON_E] =
        (new StretchButton(_buttonSize, pos,
        textures[ButtonsTextures_E::STRETCH_OBJECT_BUTTON_TEX_E], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::WALL_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::WALL_BUTTON_TEX_E],
        _editor.getWallColor(), _gameObjects[TileType::WALL_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::CHANGE_WALL_COLOR_BUTTON_E] =
        (new WallColorButton(_buttonSize, pos, 
        textures[ButtonsTextures_E::WALL_BUTTON_TEX_E],
        _editor.getWallColor(), _editor));
    advancePosition(pos, gap, alreadyInRow);


    _buttons[Buttons_E::FIRE_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::FIRE_BUTTON_TEX_E], 
        _gameObjects[TileType::FIRE_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::TELEPORT_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::TELEPORT_BUTTON_TEX_E],
        _gameObjects[TileType::TELEPORT_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::THRONE_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::THRONE_BUTTON_TEX_E],
        _gameObjects[TileType::THRONE_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::GATE_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::GATE_BUTTON_TEX_E],
        _gameObjects[TileType::GATE_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::KEY_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::KEY_BUTTON_TEX_E], 
        _gameObjects[TileType::KEY_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::ORC_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::ORC_BUTTON_TEX_E],
        _gameObjects[TileType::ORC_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::KING_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::KING_BUTTON_TEX_E],
        _gameObjects[TileType::KING_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::MAGE_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::MAGE_BUTTON_TEX_E], 
        _gameObjects[TileType::MAGE_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::THIEF_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::THIEF_BUTTON_TEX_E], 
        _gameObjects[TileType::THIEF_T], _editor));
    advancePosition(pos, gap, alreadyInRow);

    _buttons[Buttons_E::WARRIOR_BUTTON_E] =
        (new DrawingButton(_buttonSize, pos,
        textures[ButtonsTextures_E::WARRIOR_BUTTON_TEX_E],
        _gameObjects[TileType::WARRIOR_T], _editor));
    advancePosition(pos, gap, alreadyInRow);


}

void Menu::advancePosition(sf::Vector2f& pos,
    const sf::Vector2f& gap, unsigned int& alreadyInRow)
{
    pos.x += _buttonSize.x + gap.x;

    if (++alreadyInRow == NUM_OF_BUTTONS_IN_ROW)
    {
        alreadyInRow = 0;

        pos.y += _buttonSize.y + gap.y;

        // start x position at the left edge of the menu
        pos.x -= (float)(NUM_OF_BUTTONS_IN_ROW * (_buttonSize.x + gap.x));
    }
}


void Menu::setBackGround()
{
    if (BACKGROUND_TEX.loadFromFile(MENU_BG_FILE))
        setTexture(&BACKGROUND_TEX);
    else
        cerr << "problem loading menu background from file.\n\n";

}

void Menu::setButtonsBackground()
{
    if (BUTTONS_BACKGROUND_TEX.loadFromFile(BUTTONS_BG_FILE))
    {
        _buttonsBackGround.create(sf::Vector2f(BUTTON_BG_SIZE, BUTTON_BG_SIZE),
            sf::Vector2f(0, 0), &BUTTONS_BACKGROUND_TEX, BUTTON_BG_NO_FOCUS_COLOR);


        // just auxilery shape with button size for setting the background button to 
        // wrap all buttons.
        // it's better then checking if any buttons were already initialized
        // in oder to use them.. just use the auxilery..
        Shape aux(_buttonSize, { 0, 0 },TEXTURED_COLOR);
        _buttonsBackGround.wrap(aux);
    }
    else {
        cerr << "problem loading buttons background from file.\n\n";
    }
}

void Menu::draw(sf::RenderWindow& window)
{

    window.draw(toSFMLrect());


    for (unsigned int button = 0; button < _buttons.size(); ++button)
    {

        // set backGround button postion to current button position
        _buttonsBackGround.setPosition(_buttons[button]->getPosition());

        if (_buttons[button]->getFocus())
        {

            _buttonsBackGround.setColor(BUTTON_BG_FOCUS_COLOR);

        }
        else if (_buttons[button]->getPressed())
        {
            _buttonsBackGround.setColor(BUTTON_PRESSED_FOCUS_COLOR);
        }
        // draw background of button
        _buttonsBackGround.draw(window);
        _buttonsBackGround.setColor(BUTTON_BG_NO_FOCUS_COLOR);
        // draw the button itself.
        _buttons[button]->draw(window);

    }
}

void Menu::cleanMouse()
{
    for (unsigned int i = 0; i < _buttons.size(); ++i)
    {
        _buttons[i]->setInFocus(false);
        _buttons[i]->setPressed(false);
    }
}



