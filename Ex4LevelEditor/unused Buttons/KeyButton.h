#pragma once
#include "Button.h"
class KeyButton :
    public Button
{
public:
    KeyButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~KeyButton();
};

