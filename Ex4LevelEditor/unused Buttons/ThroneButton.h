#pragma once
#include "Button.h"
class ThroneButton :
    public Button
{
public:
    ThroneButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~ThroneButton();
};

