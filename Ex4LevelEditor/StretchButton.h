#pragma once
#include "Button.h"
class StretchButton :
    public Button
{
public:

    StretchButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    
    ~StretchButton();

    virtual void action();
};

