#pragma once
#include "Button.h"
class MageButton :
    public Button
{
public:
    MageButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~MageButton();
};

