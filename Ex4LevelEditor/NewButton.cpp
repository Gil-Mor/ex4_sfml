#include "NewButton.h"
#include "LevelEditor.h"

NewButton::NewButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

NewButton::~NewButton()
{
}

void NewButton::action()
{
    _editor.newBoard();
}
