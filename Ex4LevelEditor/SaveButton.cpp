#include "SaveButton.h"
#include "LevelEditor.h"

SaveButton::SaveButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

SaveButton::~SaveButton()
{
}

void SaveButton::action()
{
    _editor.saveToFile();
}
