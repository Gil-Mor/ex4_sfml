#pragma once
#include "Button.h"
class OrcButton :
    public Button
{
public:

    OrcButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~OrcButton();
};

