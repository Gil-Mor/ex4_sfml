#include "ChoiceButton.h"
#include "LevelEditor.h"

ChoiceButton::ChoiceButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

ChoiceButton::~ChoiceButton()
{
}

void ChoiceButton::action()
{
    _editor.setChoiceMode();
}


 