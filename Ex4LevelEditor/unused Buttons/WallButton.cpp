#include "WallButton.h"
#include "LevelEditor.h"

WallButton::WallButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    const sf::Color& color,
    LevelEditor& editor)
    : Button(size, position, texture, color, editor)
{
}

WallButton::~WallButton()
{
}

void WallButton::action()
{
    _editor.placeUnderCursor(TileType::Tiles_E::SINGLE_WALL_T);
}

