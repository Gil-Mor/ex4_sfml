#pragma once
#include "Button.h"
class GateButton :
    public Button
{
public:
    GateButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~GateButton();
};

