#include "DrawBoard.h"
#include "TransformableTile.h"
#include <iostream>
using std::cout;
using std::endl;

const sf::Color TILE_DRAWING_FOCUS_COLOR = { 170, 60, 60, 250 };
//const sf::Color TILE_CHOICE_FOCUS_COLOR = { 170, 60, 60, 250 };
const sf::Color TILE_NO_FOCUS_COLOR = TEXTURED_COLOR;

const sf::Color ERROR_COLOR = sf::Color::Red;

const sf::Color TILE_CHOSEN_COLOR = { 60, 40, 200, 250 };
const sf::Color BOARD_COLOR = TEXTURED_COLOR;

const sf::Color TILE_OUTLINE_COLOR = sf::Color::Black;
const float TILE_OUTLINE_THICKNESS = 1;


const unsigned int NUM_OF_TELEPORTS = 8;
const sf::Color TELEPORT_COLOR_1 = { 255, 153, 153, 255 };
const sf::Color TELEPORT_COLOR_2 = { 153, 0, 153, 255 };
const sf::Color TELEPORT_COLOR_3 = { 51, 51, 255, 255 };
const sf::Color TELEPORT_COLOR_4 = { 204, 255, 130, 255 };

DrawBoard::DrawBoard()
{
}

DrawBoard::DrawBoard(const sf::Vector2u& levelSize,
    const sf::Vector2f& size,
    const sf::Vector2f& position,
    const vector<Tile*> gameObjects)
    : _gameObjects(gameObjects), _levelSize(levelSize),
    Shape(size, position)
{
    initTileSize();
    initOnBoard();
    setBoard();
}

void DrawBoard::initOnBoard()
{
    _onBoard.king = false;
    _onBoard.mage = false;
    _onBoard.thief = false;
    _onBoard.warrior = false;
}

void DrawBoard::restart(const sf::Vector2u levelSize)
{
    _levelSize = levelSize;
    initTileSize();
    initOnBoard();
    setBoard();
}

void DrawBoard::initTileSize()
{
    //calculate tiles size according to level size and the draw board size
    //make tiles square.


    //float squareTileSize = calcSquareTileSize();

    //_tileSize.x = squareTileSize;
    //_tileSize.y = squareTileSize;

    _tileSize = calcTileSize();
}

DrawBoard::~DrawBoard()
{
    for (unsigned int i = 0; i < _board.size(); ++i)
    {
        for (unsigned int j = 0; j < _board[i].size(); ++j)
        {
            delete _board[i][j];
        }
    }
}

DrawBoard::DrawBoard(const DrawBoard& source)
    : Shape(source)
{

    *this = source;
}

DrawBoard& DrawBoard::operator=(const DrawBoard& source)
{

    Shape::operator=(source);

    _levelSize = source._levelSize;
    _tileSize = source._tileSize;

    for (unsigned int i = 0; i < source._gameObjects.size(); ++i)
    {
        _gameObjects.push_back(source._gameObjects[i]);
    }

    setBoard();
    initOnBoard();
    return *this;
}

sf::Vector2f DrawBoard::calcTileSize() const
{
    sf::Vector2f tile;
    tile.x = (float)(getSize().x / (float)_levelSize.x);
    tile.y = (float)(getSize().y / (float)_levelSize.y);
    return tile;

}

float DrawBoard::calcSquareTileSize() const
{
    float tileWidth = (float)(getSize().x / (float)_levelSize.x);
    float tileHeight = (float)(getSize().y / (float)_levelSize.y);
    float squareTileSize = tileWidth < tileHeight ? tileWidth : tileHeight;
    return squareTileSize;

}

void DrawBoard::setBoard()
{
    // start position at the top left corner
    // of the drawing board.
    sf::Vector2f tilePosition = getPosition();

    // prepare grid size
    _board.resize(_levelSize.y);

    Tile* t;
    for (unsigned int row = 0; row < _levelSize.y; ++row)
    {
        for (unsigned int col = 0; col < _levelSize.x; ++col)
        {
            //_board[row][col] = Tile(_tileSize, tilePosition);

            t = new Tile(_tileSize, tilePosition,
                _gameObjects[TileType::BLANK_T]->getTexture(), TileType::BLANK_T,
                row, col);

            t->setOutLineThickness(TILE_OUTLINE_THICKNESS);
            t->setOutLineColor(TILE_OUTLINE_COLOR);
            _board[row].push_back(t);

            // out line thickness and color are set in tile cotr..
            tilePosition.x += _tileSize.x;

        }
        // go one row down.
        tilePosition.y += _tileSize.y;

        // start over from left side.
        tilePosition.x = getPosition().x;
    }
    initOnBoard();
}

Tile* DrawBoard::getTileInFocus(
    const sf::Vector2i& mousePosition) const
{
    for (unsigned int i = 0; i < _board.size(); ++i)
    {
        for (unsigned int j = 0; j < _board[i].size(); ++j)
        {
            if (_board[i][j]->mouseHover(mousePosition))
            {

                return _board[i][j];
            }
        }
    }

    // if for some reason the mouse wasn't above any tile
    // (a small gap between the board and the menu..)
    // set it to be on arbitrary tile..
    return _board[0][0];
}

void DrawBoard::handleChoiceMovment(
    const sf::Vector2i& mousePosition)
{

    cleanMouse();

    Tile* infocus = getTileInFocus(mousePosition);
    if (infocus == nullptr)
        return;

    if (infocus->transformable())
    {
        _currentInFocus = getTransformable(infocus);
        setTilesInChoiceFocus();
        return;
    }
    _currentInFocus.push_back(infocus);
    setTilesInChoiceFocus();

}

void DrawBoard::setTilesInChoiceFocus()
{
    for (unsigned int i = 0; i < _currentInFocus.size(); ++i)
    {
        // set tile to be "in focus": change color
        _currentInFocus[i]->setInFocus(true);
        pretendWithOutTexture(_currentInFocus[i]);
    }
}

void DrawBoard::setTilesInDrawingFocus(const Tile* underCursor)
{
    for (unsigned int i = 0; i < _currentInFocus.size(); ++i)
    {
        // set tile to be "in focus": change color
        _currentInFocus[i]->setInFocus(true);
        pretendWithTexture(_currentInFocus[i], underCursor);
    }
}

void DrawBoard::clearFocus()
{
    for (unsigned int i = 0; i < _currentInFocus.size(); ++i)
    {
        _currentInFocus[i]->setInFocus(false);
    }
    _currentInFocus.clear();

}

void DrawBoard::clearChosen()
{
    for (unsigned int i = 0; i < _currentChosen.size(); ++i)
    {
        _currentChosen[i]->setChosen(false);
    }
    _currentChosen.clear();

}


void DrawBoard::handleDrawingMovment(
    const sf::Vector2i& mousePosition,
    Tile* underCursor)
{

    cleanMouse();


    Tile* infocus = getTileInFocus(mousePosition);
    //if (infocus == nullptr)
    //    return;

    _currentInFocus = getTilesunderCursorObject
        (infocus, underCursor);


    if (transformablesCollision(underCursor))
    {
        clearFocus();
        return;

    }
    if (infocus->getType() == TileType::TELEPORT_T)
    {
        movingError(infocus, underCursor);
        clearFocus();
        return;
    }


    setTilesInDrawingFocus(underCursor);

}

bool DrawBoard::transformablesCollision(Tile* underCursor)
{

    // check if one of the tiles in focus is a transformable.
    int transInFocus = containsTranformable(_currentInFocus);

    if (transInFocus > -1)
    {

        TransformableTile* tmp = getCasted(_currentInFocus[transInFocus]);
        // if both tranformables (underCursor and on board) are long
        // cant't place what's under the cursoe on the board.
        if (tmp->isLong())
        {
            movingError(_currentInFocus[transInFocus], underCursor);

            //_currentChosen.clear();
            return true;
        }

    }
    int teleportInFocus = containsTeleport(_currentInFocus);

    if (teleportInFocus > -1)
    {

        movingError(_currentInFocus[teleportInFocus], underCursor);

        return true;
    }


    return false;
}

TransformableTile* DrawBoard::getCasted(Tile* tile) const
{
    return static_cast<TransformableTile*>(tile);
}

int DrawBoard::containsTranformable(
    const vector<Tile*>& tiles)
{

    for (unsigned int i = 0; i < tiles.size(); ++i)
    {
        if (tiles[i]->transformable())
            return i;
    }
    return -1;
}

int DrawBoard::containsTeleport(const vector<Tile*>& tiles)
{

    for (unsigned int i = 0; i < tiles.size(); ++i)
    {
        if (tiles[i]->getType() == TileType::TELEPORT_T)
            return i;
    }
    return -1;
}

void DrawBoard::movingError(
    Tile* head,
    Tile* underMouse)
{

    vector<Tile*> inTrans = getTilesunderCursorObject(head, underMouse);

    for (unsigned int i = 0; i < inTrans.size(); ++i)
    {
        inTrans[i]->setColor(ERROR_COLOR);

    }

}

vector<Tile*> DrawBoard::getTilesunderCursorObject(
    Tile* startPointOnBoard,
    Tile* underCursor) const
{
    vector<Tile*> tiles;

    if (!underCursor->transformable())
    {

        if (startPointOnBoard->transformable())
        {
            return getTransformable(startPointOnBoard);
        }

        tiles.push_back(startPointOnBoard);
        return tiles;
    }

    // aux for short writing
    TransformableTile* tTile = getCasted(underCursor);

    int rowDir, colDir;
    getTransformableDirection(tTile, rowDir, colDir);

    unsigned int row = startPointOnBoard->getRow();
    unsigned int col = startPointOnBoard->getCol();

    for (unsigned int countLen = 0;
        0 <= row && row < _board.size()
        && 0 <= col && col < _board[row].size()
        && countLen < tTile->getLength();
    row += rowDir, col += colDir, countLen++)
    {
        if (_board[row][col]->transformable())
        {
            vector<Tile*> moreTrans =
                getTransformable(_board[row][col]);
            tiles.insert(tiles.end(), moreTrans.begin(), moreTrans.end());
        }

        else
            tiles.push_back(_board[row][col]);
    }

    return tiles;
}

vector<Tile*> DrawBoard::getTransformable(
    Tile* onBoard) const
{
    vector<Tile*> trans;

    Tile* head = getTransformableHead(onBoard);

    TransformableTile* tmp = getCasted(head);

    unsigned int row = tmp->getRow();
    unsigned int col = tmp->getCol();

    int rowD, colD;
    getTransformableDirection(tmp, rowD, colD);


    for (unsigned int i = 0;
        i < tmp->getLength()
        && 0 <= row && row < _board.size()
        && 0 <= col && col < _board[row].size()
        ; row += rowD, col += colD, ++i)
    {
        trans.push_back(_board[row][col]);
    }
    return trans;
}


void DrawBoard::getTransformableDirection(
    const TransformableTile* tile,
    int& row, int& col) const
{

    if (tile->getRotation() == tile->HORIZONTAL_R)
    {
        row = 0;
        col = 1;
        return;

    }

    if (tile->getRotation() == tile->VERTICAL_R)
    {
        row = 1;
        col = 0;
        return;

    }
}


void DrawBoard::handleChoiceClick(
    const sf::Vector2i& mousePosition)
{

    _currentChosen = _currentInFocus;

    if (_currentChosen[0]->getType() == TileType::BLANK_T)
    {
        clearChosen();
        clearFocus();

        return;
    }

    setTilesChosen(_currentChosen);
    clearFocus();

}

Tile* DrawBoard::getTransformableHead(Tile* tile) const
{
    int rowD, colD;
    getTransformableDirection(getCasted(tile), rowD, colD);

    bool outOfRange = false;
    unsigned int i = 0, row = tile->getRow(), col = tile->getCol();


    // go upstream until you meet the head
    for (; i < tile->getLength(); row -= rowD, col -= colD, i++)
    {
        if (_board[row][col]->transformable())
        {
            if (getCasted(_board[row][col])->getIndex() == 0)
                return _board[row][col];
        }
        else {
            outOfRange = true;
            break;
        }
    }

    // if we didn;t 
    if (!outOfRange)
    {
        getCasted(_board[row][col])->setIndex(0);
        return _board[row][col];
    }
    return tile;
}

void DrawBoard::setTilesChosen(vector<Tile*>& tiles)
{
    for (unsigned int i = 0; i < tiles.size(); ++i)
    {
        tiles[i]->setChosen(true);
        tiles[i]->setColor(TILE_CHOSEN_COLOR);
    }
}


void DrawBoard::handleDeletionClick(
    const sf::Vector2i& mousePosition)
{

    // dlelete already chosen tiles
    if (_currentChosen.empty())
    {
        // make sure they're in focus
        handleChoiceMovment(mousePosition);

        // fake a click to chose them
        handleChoiceClick(mousePosition);
    }
    if (_currentChosen.empty())
    {
        return;
    }

    deleteTilesChosen();

    clearChosen();
    clearFocus();
}

void DrawBoard::handleMenuDeletionClick()
{
    if (_currentChosen.empty())
    {
        return;
    }

    deleteTilesChosen();
    clearChosen();
    clearFocus();
}

void DrawBoard::deleteTilesChosen()
{
    for (unsigned int i = 0; i < _currentChosen.size(); ++i)
    {
        // remove characters from the board.
        if (alreadyOnBoard(_currentChosen[i]))
        {
            setIsOnBoard(_currentChosen[i], false);
        }

        _board[_currentChosen[i]->getRow()][_currentChosen[i]->getCol()]
            = newTile(_currentChosen[i], TileType::BLANK_T);

        delete _currentChosen[i];
    }
    // clear current selection.
    _currentChosen.clear();
}

Tile* DrawBoard::newTile(
    const Tile* source, TileType::Tiles_E type) const
{
    return new Tile(source->getSize(), source->getPosition(),
        _gameObjects[type]->getTexture(), type,
        source->getRow(), source->getCol());
}



// under Cursor Tile cant be const because it causesa chain
// reaction that does'nt allow to change anything..
void DrawBoard::handleDrawingClick(
    Tile* underCursor)
{



    if (_currentInFocus.empty()
        || underCursor->getType() == TileType::BLANK_T)
        return;


    // handle transformable drawing
    if (underCursor->transformable())
    {
        transformableDrawingClick(getCasted(underCursor));
    }

    // handle teleport drawing
    else if (underCursor->getType() == TileType::TELEPORT_T)
    {
       /// teleportDrawingClick(_currentInFocus[0]);
        return;
    }

    // handle simple drawings..
    else
    {
        if (alreadyOnBoard(underCursor))
        {
            movingError(_currentInFocus[0], underCursor);
            clearFocus();
            return;
        }

        unsigned int row = _currentInFocus[0]->getRow();
        unsigned int col = _currentInFocus[0]->getCol();
        makeMe(_board[row][col], underCursor);

        _board[row][col]->setOccupied(true);
        setIsOnBoard(underCursor, true);

    }


    _currentInFocus.clear();

}


bool DrawBoard::alreadyOnBoard(const Tile* underCursor) const
{
    if (underCursor->getType() == TileType::KING_T)
        return _onBoard.king;

    if (underCursor->getType() == TileType::MAGE_T)
        return _onBoard.mage;

    if (underCursor->getType() == TileType::WARRIOR_T)
        return _onBoard.warrior;

    if (underCursor->getType() == TileType::THIEF_T)
        return _onBoard.thief;

    return false;
}

void DrawBoard::setIsOnBoard(const Tile* underCursor, bool value)
{
    if (underCursor->getType() == TileType::KING_T)
        _onBoard.king = value;

    if (underCursor->getType() == TileType::MAGE_T)
        _onBoard.mage = value;

    if (underCursor->getType() == TileType::WARRIOR_T)
        _onBoard.warrior = value;

    if (underCursor->getType() == TileType::THIEF_T)
        _onBoard.thief = value;

}


void DrawBoard::transformableDrawingClick(
    TransformableTile* underCursor)
{



    for (unsigned int i = 0; i < _currentInFocus.size(); ++i)
    {
        unsigned int cellRow = _currentInFocus[i]->getRow();
        unsigned int cellCol = _currentInFocus[i]->getCol();
        _board[cellRow][cellCol] = newTransformable(_currentInFocus[i], underCursor);

        // set the tile that was clicked with the mouse to be
        // the "head" of this chain

        getCasted(_board[cellRow][cellCol])->setIndex(i);
        _board[cellRow][cellCol]->setOccupied(true);

        // free former tile.
        delete _currentInFocus[i];
    }
}

TransformableTile* DrawBoard::newTransformable(
    Tile* source, const TransformableTile* dest)
{
    TransformableTile* t = new TransformableTile(*dest);
    t->setSize(source->getSize());
    t->setPosition(source->getPosition());
    t->setRow(source->getRow());
    t->setCol(source->getCol());
    return t;

}

void DrawBoard::cleanMouse()
{
    for (unsigned int row = 0; row < _board.size(); ++row)
    {
        for (unsigned int col = 0; col < _board[row].size(); ++col)
        {

            _board[row][col]->setInFocus(false);
            if (!_board[row][col]->getChosen())
                stopPretend(_board[row][col]);
        }
    }
    _currentInFocus.clear();
}


void DrawBoard::draw(sf::RenderWindow& window)
{

    Tile bg = Tile(_tileSize, { 0, 0 },
        _gameObjects[TileType::BLANK_T]->getTexture(),
        TileType::BLANK_T, 0, 0);

    bg.setOutLineThickness(1);
    bg.setOutLineColor(sf::Color::Black);

    unsigned int x = 0;
    for (unsigned int row = 0; row < _board.size(); ++row)
    {

        for (unsigned int col = 0; col < _board[row].size(); ++col)
        {

            // draw a blank tile underneath every tile.. eliminates
            // the need to create textures with a blank tile background
            bg.setPosition(_board[row][col]->getPosition());
            bg.draw(window);

            _board[row][col]->draw(window);

        }
    }
}


void DrawBoard::makeMe(Tile* left, const Tile* right)
{
    left->setTexture(right->getTexture());
    left->setTrueTexture(right->getTexture());
    left->setColor(right->getColor());
    left->setTrueColor(right->getColor());
    left->setType(right->getType());
}

void DrawBoard::pretendWithTexture(Tile* left, const Tile* right)
{

    left->setTexture(right->getTexture());

    left->setColor(TILE_DRAWING_FOCUS_COLOR);
    left->setColor(semiColor(right));


}

void DrawBoard::pretendWithOutTexture(Tile* tile)
{
    tile->setColor(TILE_CHOSEN_COLOR);
    //tile->setColor(semiColor(tile));
}

void DrawBoard::stopPretend(Tile* left)
{
    left->restore();
    left->setColor(fullColor(left));


}

sf::Color DrawBoard::semiColor(const Tile* tile) const
{
    sf::Color semiColor = tile->getColor();
    semiColor.a = 200;
    return semiColor;
}

sf::Color DrawBoard::fullColor(const Tile* tile) const
{
    sf::Color fullColor = tile->getColor();
    fullColor.a = 255;
    return fullColor;
}



const sf::Texture* DrawBoard::typeTexture(const Tile* tile) const
{
    return _gameObjects[tile->getType()]->getTexture();
}

sf::Color DrawBoard::typeColor(const Tile* tile) const
{
    return _gameObjects[tile->getType()]->getColor();
}

sf::Vector2u DrawBoard::getLevelSize() const
{
    return _levelSize;
}

void DrawBoard::writeToFile(
    ofstream& file, const vector<string>& names) const
{

    // first write level size
    file << _levelSize.x << ", " << _levelSize.y << "\n\n";
    // initialized 2d vector of 0's to tell which tileswere written
    //used to handle long transformables..
    vector<vector<int > > writen(
        _board.size(), vector<int>(_board[0].size()));


    for (unsigned int row = 0; row < _board.size(); ++row)
    {
        for (unsigned int col = 0; col < _board[row].size(); ++col)
        {

            // skip written tiles.
            if (writen[row][col])
            {
                continue;
            }

            writeTile(_board[row][col], file, names, writen);

        }
    }
}

void DrawBoard::writeTile(
    Tile* tile, ofstream& file,
    const vector<string>& names,
    vector<vector<int> >& writen) const
{

    file << getTileName(tile, names)
        << " " << tile->getRow()
        << " " << tile->getCol();

    // if transformable and it's length is more then 1,
    // write all of it
    if (tile->transformable())
    {
        TransformableTile* tmp = getCasted(tile);

        if (tmp->getIndex() == 0)
            writeTransformable(tmp, file, writen);
    }

    writen[tile->getRow()][tile->getCol()] = 1;
    file << "\n";
}

void DrawBoard::writeTransformable(
    TransformableTile* tile,
    ofstream& file,
    vector<vector<int> >& writen) const
{

    // get all tiles belonging to this transformable
    // object to mark them as writen.
    vector<Tile*> under = getTransformable(tile);

    file << ", Length: " << tile->getLength();

    file << ", Direction: ";

    if (tile->getRotation() == tile->HORIZONTAL_R)
    {
        file << "horizontal";
    }
    else {
        file << "vertical";
    }

    if (tile->getType() == TileType::WALL_T)
    {
        file << ", Color: " << getRGBA(tile);
    }

    markAllAsWritten(under, writen);

}

string DrawBoard::getRGBA(const Tile* tile) const
{

    sf::Color tmp = tile->getColor();
    string c;

    c = "{"
        + std::to_string(tmp.r) + ", "
        + std::to_string(tmp.g) + ", "
        + std::to_string(tmp.b) + ", "
        + std::to_string(tmp.a) + "}";

    return c;
}



void DrawBoard::markAllAsWritten(
    vector<Tile*>& under, vector<vector<int> >& writen) const
{
    for (unsigned int i = 0; i < under.size(); ++i)
    {
        writen[under[i]->getRow()][under[i]->getCol()] = 1;
    }
}

string DrawBoard::getTileName(const Tile* tile, const vector<string>& names) const
{
    return names[tile->getType()];
}



