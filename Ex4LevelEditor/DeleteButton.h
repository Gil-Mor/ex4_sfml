#pragma once
#include "Button.h"
class DeleteButton : public Button
{
public:
    DeleteButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~DeleteButton();

    virtual void action();
};

