#include "ShrinkButton.h"
#include "LevelEditor.h"

ShrinkButton::ShrinkButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

ShrinkButton::~ShrinkButton()
{
}

void ShrinkButton::action()
{
    if (_editor.getUnderCorsur()->transformable())
    {
        TransformableTile* tile = 
            static_cast<TransformableTile*>(_editor.getUnderCorsur());

        if (tile->isLong())
            tile->decrementLength();
    }
}
