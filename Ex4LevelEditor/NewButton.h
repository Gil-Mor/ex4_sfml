#pragma once
#include "Button.h"
class NewButton :
    public Button
{
public:
    NewButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~NewButton();

    virtual void action();
};

