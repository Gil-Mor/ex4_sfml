#pragma once
#include "Button.h"
class WallButton :
    public Button
{
public:
    WallButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        const sf::Color& color,
        LevelEditor& editor);


    ~WallButton();

    void action();
};

