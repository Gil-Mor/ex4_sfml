#include "WallColorButton.h"
#include "LevelEditor.h"

WallColorButton::WallColorButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    const sf::Color& color,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}


WallColorButton::~WallColorButton()
{
}

void WallColorButton::action()
{
    _editor.changeWallColor();
    setColor(_editor.getWallColor());

}
