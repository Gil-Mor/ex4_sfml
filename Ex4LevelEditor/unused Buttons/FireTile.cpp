#include "FireTile.h"


FireTile::FireTile()
{
}

FireTile::FireTile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Color& color,
    TileType::Tiles_E type)
    : TransformableTile(size, position, color, type)
{
}

FireTile::FireTile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* tex,
    TileType::Tiles_E type)
    : TransformableTile(size, position, tex, type)
{
}

FireTile::~FireTile()
{
}
