#include "Button.h"


Button::Button(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
     LevelEditor& editor)
     : _editor(editor), _pressed(false),
    Shape(size, position, texture)
{
}

Button::Button(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    const sf::Color& color,
     LevelEditor& editor)
     : _editor(editor), _pressed(false),
    Shape(size, position, texture, color)
{
}


Button::~Button()
{
}

void Button::setPressed(bool p)
{
    _pressed = p;
}
bool Button::getPressed() const
{
    return _pressed;
}

void Button::action()
{
}



