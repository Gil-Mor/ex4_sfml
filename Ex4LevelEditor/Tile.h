#pragma once
#include "Shape.h"


// enum that's used by everyone.
// that's all the objects that can
// be pdrawn on the board.
namespace TileType
{
    enum Tiles_E
    {
        KING_T = 0,
        MAGE_T, // 1
        THIEF_T,
        WARRIOR_T,

        BLANK_T,
        WALL_T,
        FIRE_T,
        THRONE_T,
        GATE_T,
        KEY_T,
        ORC_T,
        TELEPORT_T, // 13


        NUM_OF_TILES_T // 14
    };
};

class Tile : public Shape
{

public:

    // again cotrs for every situation.
    Tile();

    // 2 cotrs without size and position
    // for game objects that don't have 
    // size and position..
    // i might change that.. create a GameObject class..
    Tile(const sf::Texture* tex,
        TileType::Tiles_E type,
        unsigned int row = 0,
        unsigned int col = 0);

    Tile(const sf::Texture* tex,
        const sf::Color& color,
        TileType::Tiles_E type,
        unsigned int row = 0,
        unsigned int col = 0);

    Tile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Color& color,
        TileType::Tiles_E type,
        unsigned int row,
        unsigned int col);

    Tile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* tex,
        TileType::Tiles_E type,
        unsigned int row,
        unsigned int col);

    Tile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* tex,
        const sf::Color& color,
        TileType::Tiles_E type,
        unsigned int row,
        unsigned int col);

    ~Tile();

    void setRow(unsigned int row);
    void setCol(unsigned int col);
    unsigned int getRow() const;
    unsigned int getCol() const;

    /* returns true if this tile isn't the blank tile*/
    bool getOccupied() const;

    /* set the occupiedstate. */
    void setOccupied(bool o);

    /* reurntrue if the tile was "chosen": clickedby the mouse.*/
    bool getChosen() const;

    /*set the Chosen state. */
    void setChosen(bool c);

    /* get the type of the tile. */
    TileType::Tiles_E getType() const;

    /* set the type. */
    void setType(TileType::Tiles_E type);


    // transformableTile overrides these methods.
    // all other tiles return false and 1.
    virtual bool transformable() const;
    virtual unsigned int getLength() const;

    /* restore the tiles real texture and color. */
    void restore();

    /* set the tiles fixes texture. 
    The true texture and colorallow the tile to set
    temporary texture and color.*/
    void setTrueTexture(const sf::Texture* tex);

    /* set the tiles fixed color*/
    void setTrueColor(const sf::Color color);
private:

    /* tells if the tile isn't the blank tile.*/
    bool _occupied;

    /* that if the mouse clicked on the tile. */
    bool _chosen;

    /* the tile's fixed texture and color.
     these are used to restore the tile's look
     after it was hovered*/
    const sf::Texture * _trueTexture;
    sf::Color _trueColor;

    /* The tile's type. */
    TileType::Tiles_E _type;

    // the tile's position on the board.
    struct Position
    {
        unsigned int row, col;
    };

    Position _boardPos;
};

