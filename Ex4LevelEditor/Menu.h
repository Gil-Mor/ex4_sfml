#pragma once

#include "Button.h"
#include "Shape.h"

#include <vector>

#include <SFML\Graphics.hpp>

using std::vector;

class LevelEditor;

class Menu : public Shape
{

public:


    Menu();

    Menu(const sf::Vector2f& size,
        const sf::Vector2f& position,
        LevelEditor& editor);


    ~Menu();

    void setButtons(const vector<const sf::Texture*>& textures);
    void setGameObjects(const vector<Tile*>& gameObjects);

    /* set the backgroundtexture of the menu.
    This texture is loaded independentaly from other textures.*/
    void setBackGround();

    /* set the backgroundtexture of the buttons.
    This texture is loaded independentaly from other textures.*/
    void setButtonsBackground();

    /* iterate over thebuttons and draw them. */
    void draw(sf::RenderWindow& window);

    // cleans mouse inFocus colors after it left menu..
    // sometimes, if the mouse leaves too quickly, 
    // the inFocus color can stick to a button...
    void cleanMouse();


    void handleMouseMovment(const sf::Vector2i& mousePosition);
    void handleMouseClick(const sf::Vector2i& mousePosition,
        Tile* underCursor);

    // the enum is used as indexes for the buttuns array
    enum Buttons_E
    {

        NEW_BUTTON_E = 0, // 0
        SAVE_BUTTON_E, // 1
        CHOICE_BUTTON_E, // 2
        DELETE_OBJECT_BUTTON_E, // 3
        STRETCH_OBJECT_BUTTON_E, // 4
        SHRINK_OBJECT_BUTTON_E, // 5
        ROTATE_BUTTON_E, // 6

        KING_BUTTON_E, // 7
        MAGE_BUTTON_E, // 8
        THIEF_BUTTON_E, // 9
        WARRIOR_BUTTON_E, // 10


        WALL_BUTTON_E, // 11
        CHANGE_WALL_COLOR_BUTTON_E, // 12
        FIRE_BUTTON_E, // 13
        THRONE_BUTTON_E, // 14
        GATE_BUTTON_E, // 15
        KEY_BUTTON_E, // 16
        ORC_BUTTON_E, // 17
        TELEPORT_BUTTON_E, // 18


        NUM_OF_BUTTONS_E // 19
    };
    
    // the enum is used as indexes for the texture array
    // there is one texture less then the buttons
    // since wall color and wall button use the 
    // same one.
    enum ButtonsTextures_E
    {
        NEW_BUTTON_TEX_E = 0, // 0
        SAVE_BUTTON_TEX_E, // 1
        CHOICE_BUTTON_TEX_E, // 2
        DELETE_OBJECT_BUTTON_TEX_E, // 3
        STRETCH_OBJECT_BUTTON_TEX_E, // 4
        SHRINK_OBJECT_BUTTON_TEX_E, // 5
        ROTATE_BUTTON_TEX_E, // 6

        KING_BUTTON_TEX_E, // 7
        MAGE_BUTTON_TEX_E, // 8
        THIEF_BUTTON_TEX_E, // 9
        WARRIOR_BUTTON_TEX_E, // 10


        WALL_BUTTON_TEX_E, // 11
        FIRE_BUTTON_TEX_E, // 12
        THRONE_BUTTON_TEX_E, // 13
        GATE_BUTTON_TEX_E, // 14
        KEY_BUTTON_TEX_E, // 15
        ORC_BUTTON_TEX_E, // 16
        TELEPORT_BUTTON_TEX_E, // 17


        NUM_OF_BUTTONS_TEX_E // 18
    };


private:

    /* also game objects from LevelEditor.
    used to give drawing buttons their Tile.*/
    vector<Tile*> _gameObjects;

    LevelEditor& _editor;

    vector<Button*> _buttons;

    Shape  _buttonsBackGround;
    sf::Texture BACKGROUND_TEX;
    sf::Texture BUTTONS_BACKGROUND_TEX;
    sf::Vector2f _buttonSize;

    // when building button layout. This function advances
    // the position and returns the position for the next
    // button by reference.
    void advancePosition(sf::Vector2f& pos,
        const sf::Vector2f& gap, unsigned int& alreadyInRow);
};

