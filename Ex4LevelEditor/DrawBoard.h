#pragma once
#include "Shape.h"
#include "Tile.h"
#include "TransformableTile.h"
#include "TeleportTile.h"
#include <fstream>
#include <vector>
using std::vector;
using std::string;
using std::ofstream;

class DrawBoard : public Shape
{
public:

    //============ COTRS ===============
    DrawBoard();

    DrawBoard(const sf::Vector2u& levelSize,
        const sf::Vector2f& size,
        const sf::Vector2f& position,
        const vector<Tile*> gameObjects);

    DrawBoard(const DrawBoard& source);
    ~DrawBoard();
    //------------  cotrs -------------

    //=============INITIALIZATION ========


    void restart(const sf::Vector2u levelsize);
    sf::Vector2u getLevelSize() const;

    void setBoard();
    //------------initialization--------

    //============ DRAWING =========


    /*Iterate over the board and draw each tile.*/
    void draw(sf::RenderWindow& window);

    //========== PUBLIC EVENTS HANDLERS==========
    void DrawBoard::handleDrawingMovment(
        const sf::Vector2i& mousePosition,
        Tile* underCursor);

    void DrawBoard::handleChoiceMovment(
        const sf::Vector2i& mousePosition);

    void handleChoiceClick(const sf::Vector2i& mousePosition);

    void handleDeletionClick(const sf::Vector2i& mousePosition);


    // under Cursor Tile cant be const because it causesa chain
    // reaction that does'nt allow to change anything..
    void handleDrawingClick(Tile* underCursor);

    /* clean the mouse focus after it leaves the board completely*/
    void cleanMouse();


    void handleMenuDeletionClick();
    //--------- public events handlers --------------

    //-----------drawing-------------------------

    DrawBoard& operator=(const DrawBoard& source);

    void writeToFile(ofstream& file, const vector<string>& names) const;


    //---------------PRIVATE---------------------
private:

    //================== MEMBERS ===================
    // the different game objects the board can contain..
    vector<Tile*> _gameObjects;

    vector<sf::Color> _wallColors;
    // the 2d  where each shape is a tile
    vector<vector<Tile*> > _board;

    vector<Tile*> _currentChosen;
    vector<Tile*> _currentInFocus;


    // number of rows and columns in the board.
    sf::Vector2u _levelSize;
    // size of tiles
    sf::Vector2f _tileSize;

    struct OnBoard
    {
        bool king, mage, thief, warrior;
    } _onBoard;


    void initOnBoard();
    bool alreadyOnBoard(const Tile* underCursor) const;
    void DrawBoard::setIsOnBoard(const Tile* underCursor, bool value);
    //-----------------members--------------------

    //=================INITIALIZATION=================


    // calculate tile's size according to the given level size.
    void DrawBoard::initTileSize();

    float calcSquareTileSize() const;

    sf::Vector2f calcTileSize() const;

    //----------------initialization--------------------


    //================ DRAWING ========================
    void clearChosen();

    void setTilesInDrawingFocus(const Tile* underCursor);
    void DrawBoard::clearFocus();
    void setTilesChosen(vector<Tile*>& tiles);

    void DrawBoard::deleteTilesChosen();
    Tile* newTile(
        const Tile* source, TileType::Tiles_E type) const;

    bool transformablesCollision(Tile* underMouse);
    int containsTeleport(const vector<Tile*>& tiles);
    /* Delete Tiles from classes derived from Tile class.
    They hold more information and need to be destroyed
    when deleted.
    */
    void DrawBoard::deleteComplexTiles();

    /* Delete Tiles from Tile class. It's enough to change
    their basic variables like type, texture, and color
    for them to become blank tiles again*/
    void DrawBoard::deleteSimpleTiles();

    Tile* DrawBoard::getTileInFocus(
        const sf::Vector2i& mousePosition) const;

    void transformableMovment(unsigned int row, unsigned int col,
        const TransformableTile* underCursor);


    void getTransformableDirection(
        const TransformableTile* tile,
        int& row, int& col) const;


    void transformableDrawingClick(
        TransformableTile* underCursor);

    int containsTranformable(
        const vector<Tile*>& onBoard);

    void movingError(Tile* head, Tile* underCursor);

    vector<Tile*> DrawBoard::getTilesunderCursorObject(
        Tile* startPointOnBoard,
        Tile* underCursor) const;

    vector<Tile*> DrawBoard::getTransformable(
        Tile* onBoard) const;

    Tile* getTransformableHead(Tile* tile) const;

    void DrawBoard::setTilesInChoiceFocus();

    void DrawBoard::markAllAsWritten(
        vector<Tile*>& under, vector<vector<int> >& writen) const;

    void cleanBeforeMovment();


    void makeMe(Tile* left, const Tile* right);

    void pretendWithTexture(Tile* left, const Tile* right);
    void pretendWithOutTexture(Tile* tile);
    void stopPretend(Tile* tile);

    TransformableTile* newTransformable(
        Tile* source, const TransformableTile* dest);

    const sf::Texture* typeTexture(const Tile* tile) const;

    sf::Color DrawBoard::typeColor(const Tile* tile) const;

    sf::Color DrawBoard::semiColor(const Tile* tile) const;

    sf::Color DrawBoard::fullColor(const Tile* tile) const;


    void writeTransformable(
        TransformableTile* tile,
        ofstream& file,
        vector<vector<int> >& writen) const;

    void writeTile(
        Tile* tile, ofstream& file,
        const vector<string>& names,
        vector<vector<int> >& writen) const;

    string getTileName(
        const Tile* tile, const vector<string>& names) const;

    string getRGBA(const Tile* tile) const;

    TransformableTile* getCasted(Tile* tile) const;
};

