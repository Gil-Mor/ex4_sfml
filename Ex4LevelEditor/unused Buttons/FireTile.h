#pragma once
#include "TransformableTile.h"
class FireTile : public TransformableTile
{
public:
    FireTile();

    FireTile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Color& color,
        TileType::Tiles_E);

    FireTile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* tex,
        TileType::Tiles_E);

    ~FireTile();
};

