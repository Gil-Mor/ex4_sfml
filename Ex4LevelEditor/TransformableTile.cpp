#include "TransformableTile.h"


TransformableTile::TransformableTile()
{
}

TransformableTile::TransformableTile(const sf::Texture* tex,
    TileType::Tiles_E type)
    : Tile(tex, type), _length(1), _rotation(HORIZONTAL_R)
{
}

TransformableTile::TransformableTile(const sf::Texture* tex,
    const sf::Color& color,
    TileType::Tiles_E type)
    : Tile(tex, color, type), _length(1), _rotation(HORIZONTAL_R)
{
}

TransformableTile::TransformableTile(
    const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Color& color,
    TileType::Tiles_E type,    
    unsigned int row,
    unsigned int col)
    : Tile(size, position, color, type, row, col),
    _length(1), _rotation(HORIZONTAL_R)
{
}

TransformableTile::TransformableTile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* tex,
    TileType::Tiles_E type,
    unsigned int row,
    unsigned int col)
    : Tile(size, position, tex, type, row, col),
    _length(1), _rotation(HORIZONTAL_R)
{
}

TransformableTile::TransformableTile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* tex,
    const sf::Color& color,
    TileType::Tiles_E type,
    unsigned int row,
    unsigned int col)
    : Tile(size, position, tex, color, type, row, col), 
    _length(1), _rotation(HORIZONTAL_R)
{
}

TransformableTile::~TransformableTile()
{
}

void TransformableTile::setIndex(unsigned int index)
{
    _index = index;
}

unsigned int TransformableTile::getIndex() const
{
    return _index;
}


void TransformableTile::initLength()
{
    _length = 1;
}

unsigned int TransformableTile::getLength() const
{
    return _length;
}

void TransformableTile::incrementLength()
{
    ++_length;
}

void TransformableTile::decrementLength()
{
    --_length;
}

void TransformableTile::rotate()
{

    switch (_rotation)
    {
        case HORIZONTAL_R:
            _rotation = VERTICAL_R;
            break;

        case VERTICAL_R:
            _rotation = HORIZONTAL_R;
            break;

        default:
            break;
    }

    setSize({ getSize().y, getSize().x });
}

TransformableTile::Rotation_E TransformableTile::getRotation() const
{
    return _rotation;
}

bool TransformableTile::transformable() const
{
    return true;
}

bool TransformableTile::isLong() const
{
    return _length > 1;
}
