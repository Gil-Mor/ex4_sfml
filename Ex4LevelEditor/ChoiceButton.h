#pragma once
#include "Button.h"
class ChoiceButton : public Button
{
public:

    ChoiceButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~ChoiceButton();

    virtual void action();
};

