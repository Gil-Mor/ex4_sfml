#pragma once
#include "Button.h"
class KingButton :
    public Button
{
public:
    KingButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~KingButton();
};

