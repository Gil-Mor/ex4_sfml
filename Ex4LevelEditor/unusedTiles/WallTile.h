#pragma once
#include "TransformableTile.h"
class WallTile : public TransformableTile
{
public:
    WallTile();

    WallTile(const sf::Texture* tex,
        const sf::Color& color,
        TileType::Tiles_E type);

    WallTile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* tex,
        const sf::Color& color,
        TileType::Tiles_E type);

    ~WallTile();

    //void setThisTileColor(const sf::Color& color);

    sf::Color getThisTileColor() const;

private:

    sf::Color _thisTileColor;

};

