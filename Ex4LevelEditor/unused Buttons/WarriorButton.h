#pragma once
#include "Button.h"
class WarriorButton :
    public Button
{
public:
    WarriorButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~WarriorButton();
};

