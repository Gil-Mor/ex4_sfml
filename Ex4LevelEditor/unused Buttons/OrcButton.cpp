#include "OrcButton.h"


OrcButton::OrcButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

OrcButton::~OrcButton()
{
}
