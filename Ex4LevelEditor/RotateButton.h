#pragma once
#include "Button.h"
class RotateButton :
    public Button
{
public:
    RotateButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~RotateButton();

    void action();
};

