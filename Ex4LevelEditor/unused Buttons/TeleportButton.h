#pragma once
#include "Button.h"
class TeleportButton :
    public Button
{
public:
    TeleportButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~TeleportButton();
};

