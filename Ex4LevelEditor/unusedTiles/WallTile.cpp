#include "WallTile.h"


WallTile::WallTile()
{
}

WallTile::WallTile(const sf::Texture* tex,
    const sf::Color& color,
    TileType::Tiles_E type)
    : _thisTileColor(color),
    TransformableTile(tex, color, type)
{
}

WallTile::WallTile(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* tex,
    const sf::Color& color,
    TileType::Tiles_E type)
    : _thisTileColor(color),
    TransformableTile(size, position, tex, type)
{
}

WallTile::~WallTile()
{
}

//void WallTile::setThisTileColor(const sf::Color& color)
//{
//    _thisTileColor = color;
//}

sf::Color WallTile::getThisTileColor() const
{
    return _thisTileColor;
}

