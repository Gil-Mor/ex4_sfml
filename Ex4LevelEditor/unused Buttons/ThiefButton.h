#pragma once
#include "Button.h"
class ThiefButton :
    public Button
{
public:
    ThiefButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~ThiefButton();
};

