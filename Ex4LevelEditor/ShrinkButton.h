#pragma once
#include "Button.h"
class ShrinkButton :
    public Button
{
public:
    ShrinkButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~ShrinkButton();

    virtual void action();
};

