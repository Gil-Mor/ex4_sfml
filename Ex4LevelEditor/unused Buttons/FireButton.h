#pragma once
#include "Button.h"

class FireButton : public Button
{
public:

    FireButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~FireButton();

};

