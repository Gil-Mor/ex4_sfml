#include "DrawingButton.h"
#include "LevelEditor.h"

DrawingButton::DrawingButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    Tile* type,
    LevelEditor& editor)
    : Button(size, position, texture, editor), _type(type)
{
}

DrawingButton::DrawingButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    const sf::Color& color,
    Tile* type,
    LevelEditor& editor)
    : Button(size, position, texture, editor), _type(type)
{
}

DrawingButton::~DrawingButton()
{
}

void DrawingButton::action()
{
    _editor.placeUnderCursor(_type);
    _editor.setDrawingMode();
    
}
