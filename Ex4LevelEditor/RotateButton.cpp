#include "RotateButton.h"
#include "LevelEditor.h"

RotateButton::RotateButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

RotateButton::~RotateButton()
{
}

void RotateButton::action()
{
    if (_editor.getUnderCorsur()->transformable())
    {
        (static_cast<TransformableTile*>(_editor.getUnderCorsur()))->rotate();
    }
}
