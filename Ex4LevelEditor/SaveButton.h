#pragma once
#include "Button.h"
class SaveButton :
    public Button
{
public:

    SaveButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture,
        LevelEditor& editor);

    ~SaveButton();

    virtual void action();
};

