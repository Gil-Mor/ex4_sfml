#pragma once
#include "Button.h"
#include <vector>
using std::vector;

class WallColorButton :
    public Button
{
public:

    WallColorButton(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* texture, 
        const sf::Color& color,
        LevelEditor& editor);

    
    ~WallColorButton();

    void action();

};

