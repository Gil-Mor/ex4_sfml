#pragma once
#include "Tile.h"
class TransformableTile : public Tile
{
public:

    /* the tiles rotation. 
    horizontal by default. */
    enum Rotation_E
    {
        HORIZONTAL_R,
        VERTICAL_R,
    };

    
    TransformableTile();

    TransformableTile(const sf::Texture* tex,
        TileType::Tiles_E type);

    TransformableTile(const sf::Texture* tex,
        const sf::Color& color,
        TileType::Tiles_E type);

    TransformableTile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Color& color,
        TileType::Tiles_E type,
        unsigned int row,
        unsigned int col);

    TransformableTile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* tex,
        TileType::Tiles_E type,
        unsigned int row,
        unsigned int col);

    TransformableTile(const sf::Vector2f& size,
        const sf::Vector2f& position,
        const sf::Texture* tex,
        const sf::Color& color,
        TileType::Tiles_E type,
        unsigned int row,
        unsigned int col);

    ~TransformableTile();

    /* return the length of this transformable. */
    void initLength();

    /* overrides this function in Tile. returns true*/
    virtual bool transformable() const;

    /* return true if the objects length is more then 1. 
    the length is the samein all tiles belonging to this
    transformable.*/
    bool isLong() const;

    /* increase length by 1. */
    void incrementLength();

    /* decrement length by 1.*/
    void decrementLength();

    /* rotatte the tile.*/
    void rotate();

    /* overrides this function in Tile. 
    intile it returns 1. here it returns the object's
    length. */
    virtual unsigned int getLength() const;

    /* set the index of this tile inside the object. */
    void setIndex(unsigned int index);

    /* get the index of the object inside the object. */
    unsigned int getIndex() const;

    /* get the object current rotation state. */
    TransformableTile::Rotation_E getRotation() const;

private:

    // number of tiles from start position until the
    // end in the Tile's direction.
    unsigned int _length;

    // this tile place in the object.
    unsigned int _index;

    /* the object's rotation state. */
    Rotation_E _rotation;


};

