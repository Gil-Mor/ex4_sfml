#include "StretchButton.h"
#include "LevelEditor.h"

StretchButton::StretchButton(const sf::Vector2f& size,
    const sf::Vector2f& position,
    const sf::Texture* texture,
    LevelEditor& editor)
    : Button(size, position, texture, editor)
{
}

StretchButton::~StretchButton()
{
}

void StretchButton::action()
{
    if (_editor.getUnderCorsur()->transformable())
    {
        (static_cast<TransformableTile*>(_editor.getUnderCorsur()))->incrementLength();
    }
}
